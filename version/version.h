#ifndef INCLUDED_VERSION_H_
#define INCLUDED_VERSION_H_

#include "../VERSION"

namespace Icmbuild
{
    char const version[]    = VERSION;
    char const years[]      = YEARS;
}


#endif
