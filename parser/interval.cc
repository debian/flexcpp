#define XERR
#include "parser.ih"

FPattern Parser::interval(FPattern &regex, Interval const &interval)
{
    Options::regexCall("interval");

    d_scanner.multiAsChar();
    return FPattern::interval(d_states, regex, interval);
}
    
