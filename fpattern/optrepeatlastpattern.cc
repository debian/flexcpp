#include "fpattern.ih"

FPattern FPattern::optRepeatLastFPattern(States &states, 
                        FPattern &fpattern,
                        size_t lower, PairVector &beginEnd)
{
    FPattern last(beginEnd.back());

        // change the last fpattern to fpattern+
        // update the begin- and end-indices
    beginEnd.back() = plus(states, last).d_pair;

    join(states, fpattern, lower, beginEnd);

    FPattern ret( {fpattern.begin(), fpattern.end()} );
    return ret;
}
