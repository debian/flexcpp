#include "fpattern.ih"

void FPattern::join(States &states, FPattern &fpattern, size_t upper,
                        PairVector const &beginEnd)
{
    FPattern next;

    for (size_t idx = 1; idx != upper; ++idx)
    {
        next.d_pair = beginEnd[idx];
        fpattern = concatenate(states, fpattern, next);
    }
}
