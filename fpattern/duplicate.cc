#include "fpattern.ih"

// Returns a FPattern which is a duplicate of the current fpattern. It's a
// self-supporting fpattern, i.e., in the returned fpattern all its next1/next2
// links refer to the new fpattern's states, and the StateData elements are
// deep copies.

FPattern FPattern::duplicate(States &states) const
{
    Map map;
    map[0] = 0;             // the end link remains as-is and is implicitly
                            // available 

    // the pair-indices of the new fpattern are found in map[d_pair.first]
    // and map[d_pair.second];

    dupFPattern(map, states, d_pair.first);

    return FPattern(Pair(map[d_pair.first], map[d_pair.second]));
}
