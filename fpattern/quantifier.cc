#include "fpattern.ih"

FPattern FPattern::quantifier(States &states, FPattern const &pat, 
                                                  size_t type)
{
    Options::regexCall("quantifier");

    FPattern ret;

    switch (type)
    {
        case '*':
            ret = star(states, pat);
        break;

        case '+':
            ret = plus(states, pat);
        break;

        case '?':
            ret = questionMark(states, pat);
        break;
    }

    return ret;
}
