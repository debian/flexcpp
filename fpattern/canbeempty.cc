#include "fpattern.ih"

bool FPattern::canBeEmpty(States const &states) const
{
    set<size_t> indices;

    return 
        (fixedLength() && length() == 0)
        or
        empty(indices, states, d_pair.first);
}
