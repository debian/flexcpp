#include "fpattern.ih"

FPattern FPattern::star(States &states, FPattern const &fpattern)
{
    Pair pair = states.next2();     // create new Start/Final states

        // fpattern's end connects to fpattern begin and the new FINAL state.
    states[fpattern.end()] = State(EMPTY, fpattern.begin(), pair.second);
    
        // new Start state connects to fpattern begin + end
    states[pair.first] = State(EMPTY, fpattern.begin(), pair.second);

    FPattern ret(pair);

    return ret;
}



