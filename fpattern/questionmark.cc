#include "fpattern.ih"

FPattern FPattern::questionMark(States &states, FPattern const &fpattern)
{
    size_t idx = states.next();     // new start state

        // connect the new Start state with the fpattern and FINAL 
    states[idx] = State(EMPTY, fpattern.begin(), fpattern.end());

    FPattern ret({idx, fpattern.end()});

    return ret;
}
