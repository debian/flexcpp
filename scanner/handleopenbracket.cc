#include "scanner.ih"

int Scanner::handleOpenBracket()
{
    if (startCondition() == StartCondition_::INITIAL)
    {
        if (d_inCharClass)
            return '[';

        d_inCharClass = true;
    }
    return Tokens::CC_START;
}

