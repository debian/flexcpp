#include "scanner.ih"

int Scanner::handleCharAfterBlanks()
{
    redo(1);

    if (d_inCharClass)
        return Tokens::CHAR;

    if (d_inBlock)
        return ' ';

    d_inBlock = true;
    d_caseSensitive = true;
    return Tokens::BLOCK;
}
