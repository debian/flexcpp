#include "scanner.ih"

string Scanner::rawStringContent() const
{
    //          d_rawstring =  )..."
    //          raw string is: R"...( <-- content --> )..."
    //          so length - (2 x d_rawstring.length() + 1) is the 
    //          content length

    return matched().substr(
                d_rawString.length() + 1,                   // + 1 for the R
                length() - ((d_rawString.length() << 1) + 1)
            );
}
