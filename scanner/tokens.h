#ifndef INCLUDED_TOKENS_
#define INCLUDED_TOKENS_

struct Tokens
{
    // Symbolic tokens:
    enum Tokens_
    {
        EXCL_START_CONDITION = 257,
        INCL_START_CONDITION,
        SECTION_DELIMITER,
        BASECLASSHEADER,
        BASECLASSPREINCLUDE,
        CASEINSENSITIVE,
        CLASSHEADER,
        CLASSNAME,
        DEBUG,
        FILENAMES,
        IMPLEMENTATIONHEADER,
        INPUTIMPLEMENTATION,
        INPUTINLINE,
        INPUTINTERFACE,
        INTERACTIVE,
        LEXFUNCTIONNAME,
        LEXSOURCE,
        NAMESPACE,
        NOLINES,
        PRINT,
        SKELETON_DIRECTORY,
        STARTCONDITION,
        TARGET_DIRECTORY,
        BLOCK,
        IDENTIFIER,
        EOF_PATTERN,
        RAWSTRING,
        STRING,
        QUOTES,
        DECIMAL,
        DOLLAR,
        ESCAPE_SEQUENCE,
        CC_START,
        CC_NEGATED,
        PREDEFINED_CLASS,
        ORNL,
        CHAR,
        CC_PLUS,
        CC_MINUS,
    };

};

#endif
